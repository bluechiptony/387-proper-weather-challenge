//
//  CityForecastViewer.swift
//  WeatherMe
//
//  Created by Anthony Egwu on 07/12/2014.
//  Copyright (c) 2014 EC. All rights reserved.
//

import UIKit

class CityForecastViewer: UIViewController, UIGestureRecognizerDelegate{
    

    var cityName: String?

    
    @IBOutlet weak var bigCityLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var minTemp: UILabel!
    
    @IBOutlet weak var maxTemp: UILabel!
    
    @IBOutlet weak var dayTemp: UILabel!
    
    @IBOutlet weak var nightTemp: UILabel!
    
    @IBOutlet weak var eveTemp: UILabel!
    
    @IBOutlet weak var mornTemp: UILabel!
    
    
    @IBAction func doneWithView(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let name = self.cityName {
            println("city Name: \(name)")
            loadCity()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadCity(){
        self.bigCityLabel.text = self.cityName
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
